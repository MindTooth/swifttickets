// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "SwiftTickets",
    targets: [
        .target(
            name: "SwiftTickets"
        ),
        .testTarget(
            name: "SwiftTicketsTests"
        ),
    ]
)
