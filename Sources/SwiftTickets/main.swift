//
//  main.swift
//  SwiftTickets
//
//  Created by Birger J. Nordølum on 15/07/2019.
//

import Foundation

var kunder = [Kunde]()

// MARK: Testing

kunder.append(Kunde(id: Konstanter.SisteKunde, navn: "Birger J. Nordølum", adresse: "Fåvang", postnummer: 2624))
Konstanter.SisteKunde += 1
kunder.append(Kunde(id: Konstanter.SisteKunde, navn: "Ole Marting Ruud", adresse: "Arendal", postnummer: 4844))

func inputFunction() -> String? {
    print("Enter choice: ", terminator: "")
    return readLine()!
}

func start() {
    print(Konstanter.Meny)

    while let input = inputFunction() {
        if input.uppercased() == "Q" {
            return
        }

        // Se Bin.swift

        switch input.uppercased() {
        case "K":
            print("Kunde")
            for kunde in kunder {
                print(kunde)
            }
        case "A":
            print("Arrangement")
        case "M":
            print(Konstanter.Meny)
        case "Q":
            return
        default:
            print("Tast inn valg på nytt!")
        }
    }
}

start()
