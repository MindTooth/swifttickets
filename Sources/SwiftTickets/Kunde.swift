//
//  Kunde.swift
//  SwiftTickets
//
//  Created by Birger J. Nordølum on 15/07/2019.
//

import Foundation

struct Kunde {
    private var id: Int
    private var navn: String
    private var adresse: String
    private var postnummer: Int

    init(id: Int, navn: String, adresse: String, postnummer: Int) {
        self.id = id
        self.navn = navn
        self.adresse = adresse
        self.postnummer = postnummer
    }
}

extension Kunde: CustomDebugStringConvertible {
    var debugDescription: String {
        return "Point(x: \(id), y: \(navn))"
    }
}

extension Kunde: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id.hashValue)
    }

    static func == (lhs: Kunde, rhs: Kunde) -> Bool {
        return lhs.id == rhs.id
    }
}
