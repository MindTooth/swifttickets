// https://www.luby.info/2015/02/15/swift-constants-files.html

struct Konstanter {
    static let NameLength = 32

    struct Stoler {
        static let Rader = 50
        static let Stoler = 100
    }

    static let Meny = """


        K - Kunde
            D - Display
            N - Ny
            E - Endre
        S - Sted
            D - Display
            N - Ny
        O - Oppsett
            D - Display
            N - Ny
            E - Endre
        A - Arrangement
            D - Display
            N - Ny
            E - Endre
            S - Slett
            K - Kjop
        M - Meny
        Q - Avslutt


    """

    static var SisteKunde: Int = 0
}
