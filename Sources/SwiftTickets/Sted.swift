//
//  Sted.swift
//  SwiftTickets
//
//  Created by Birger J. Nordølum on 15/07/2019.
//

import Foundation

struct Sted: Hashable {
    var navn: String
    var muligeOppsett = [Oppsett]()

    func hash(into hasher: inout Hasher) {
        hasher.combine(navn.hashValue)
    }

    static func == (lhs: Sted, rhs: Sted) -> Bool {
        return lhs.navn == rhs.navn
    }
}

struct Oppsett {
    var soner = [Sone]()

    func display() {
        for sone in soner {
            sone.display()
        }
    }
}

enum Sone {
    case stoler(rader: Int, stoler: Int)
    case vrimle

    func display() {
        switch self {
        case let .stoler(rader, stoler):
            print("\(rader), \(stoler)")
        case .vrimle:
            print("Vrimle")
        }
    }
}
