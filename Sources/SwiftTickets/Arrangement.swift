//
//  Arrangement.swift
//  SwiftTickets
//
//  Created by Birger J. Nordølum on 15/07/2019.
//

import Foundation

enum ArrangementType {
    case familie
    case festival
    case kino
    case musikk
    case show
    case sport
    case teater
}

struct Arrangement {
    var arrangementId = 0
    /* let sted: Sted? */
    var arrangemntType: ArrangementType?
    var tidspunkt: Date = Date()
}
