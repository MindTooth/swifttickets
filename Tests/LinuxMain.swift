import XCTest

import SwiftTicketsTests

var tests = [XCTestCaseEntry]()
tests += SwiftTicketsTests.allTests()
XCTMain(tests)
